// NAVIGATION
export const NAV_LINKS = [
    { href: '/', key: 'home', label: 'Home' },
    { href: '/', key: 'how_hilink_work', label: 'Bootcamp' },
    { href: '/', key: 'services', label: 'Services' },
    { href: '/', key: 'pricing ', label: 'Pricing ' },
    { href: '/', key: 'contact_us', label: 'Contact Us' },
  ];
  
  // CAMP SECTION
  export const PEOPLE_URL = [
    '/person-1.png',
    '/person-2.png',
    '/person-3.png',
    '/person-4.png',
  ];
  
  // FEATURES SECTION
  export const FEATURES = [
    {
      title: 'Frontend Development',
      icon: '/map.svg',
      variant: 'green',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in tempus tortor, nec euismod diam. Vestibulum maximus varius erat aliquam fringilla.',
    },
    {
      title: 'Backend Development',
      icon: '/calendar.svg',
      variant: 'green',
      description:
        "Aenean et est dolor. Nunc vel urna vitae justo pretium scelerisque dapibus et risus. Nullam malesuada consequat efficitur. ",
    },
    {
      title: 'Business Analyst',
      icon: '/tech.svg',
      variant: 'green',
      description:
        'Quisque a felis vitae nulla venenatis blandit. Nam ac ipsum lorem. Duis lobortis sollicitudin scelerisque. Phasellus nec tortor turpis.',
    },
    {
      title: 'Quality Assurance',
      icon: '/location.svg',
      variant: 'orange',
      description:
        'Fusce aliquam augue vitae sem auctor viverra. Nulla quis lectus a diam luctus vulputate molestie vitae dolor. Proin luctus vehicula vestibulum.',
    },
  ];
  
  // FOOTER SECTION
  export const FOOTER_LINKS = [
    {
      title: 'Learn More',
      links: [
        'About Hilink',
        'Press Releases',
        'Environment',
        'Jobs',
        'Privacy Policy',
        'Contact Us',
      ],
    },
    {
      title: 'Our Community',
      links: ['Climbing xixixi', 'Hiking hilink', 'Hilink kinthill'],
    },
  ];
  
  export const FOOTER_CONTACT_INFO = {
    title: 'Contact Us',
    links: [
      { label: 'Admin Officer', value: '123-456-7890' },
      { label: 'Email Officer', value: 'info@aigen-global.com' },
    ],
  };
  
  export const SOCIALS = {
    title: 'Social',
    links: [
      '/facebook.svg',
      '/instagram.svg',
      '/twitter.svg',
      '/youtube.svg',
    ],
  };