import Image from "next/image"

const Guide = () => {
  return (
    <section className="flexCenter flex-col">
      <div className="padding-container max-container w-full pb-24">
      <Image
        src="/camp.svg"
        alt="camp"
        width={50}
        height={50}
      />
      <p className="uppercase regular-18 -mt-1 mb-3 text-green-50">
        We are here for you
      </p>
        <div className="flex flex-wrap justify-between gap-5 lg:gap-10">
          <h2 className="bold-40 lg:bold-50 xl:max-w-[390px]">Guide You to Master Fullstack Development</h2>
          <p className="regular-16 text-gray-30 xl:max-w-[520px]">Morbi volutpat augue ac augue placerat, laoreet   pulvinar mi bibendum. Suspendisse lacus ante, condimentum vitae tristique sit amet, vestibulum ac   ipsum. Fusce aliquam augue vitae sem auctor viverra. Nulla quis lectus a diam luctus vulputate molestie   vitae dolor. Proin luctus vehicula vestibulum. Aliquam mollis maximus metus, nec cursus lectus  imperdiet quis. Sed magna nibh, tincidunt eget nibh a, commodo posuere est.</p>
        </div>
      </div>
      <div className="flexCenter max-container relative w-full">
        <Image
          src="/boat.png"
          alt="boat"
          width={1440}
          height={580}
          className="w-full object-cover object-center 2xl:rounded-5xl"
        />
        <div className="absolute flex bg-white py-8 pl-5 pr-7 gap-3 rounded-3xl border shadow-md md:left-[5%] lg:top-20">
          <Image
            src="/meter.svg"
            alt="meter"
            width={16}
            height={158}
            className="h-full w-auto"
          />
          <div className="flexBetween flex-col">
            <div className="flex w-full flex-col">
              <div className="flexBetween w-full">
                <p className="regular-16 text-gray-20">Goal</p>
                <p className="bold-16 text-green-50">Fullstack Master</p>
              </div>
              <p className="bold-20 mt-2">Fullstack Development</p>
            </div>
            <div className="flex w-full flex-col">
                <p className="regular-16 text-gray-20">Start</p>
                <p className="bold-20 mt-2 whitespace-nowrap">May 2024</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Guide